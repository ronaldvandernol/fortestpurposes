﻿CREATE PROCEDURE [dbo].[updHistBkEntis]
AS
BEGIN
	DECLARE @dtstart datetime
	DECLARE @dtend datetime
	
	SET @dtstart = '2012-2-1';
	SET @dtend = '2012-02-26';

	WHILE @dtstart <= @dtend
	BEGIN
		update BoekvoorraadTank  
		set Entis = (Select top 1 DCS_Tank_gauge_trail.Gross_std_vol FROM  
		DCS_Tank_gauge_trail   
		WHERE (DCS_Tank_gauge_trail.Tank=BoekvoorraadTank.Tank) AND  
		DCS_Tank_gauge_trail.Entry_date >= DATEADD(day,-2,BoekvoorraadTank.Datum) AND  
		DCS_Tank_gauge_trail.Entry_date <= DATEADD(minute,1,BoekvoorraadTank.Datum) ORDER BY DCS_Tank_gauge_trail.Entry_date DESC
		), 
		EntisKG = (Select top 1 DCS_Tank_gauge_trail.Product_mass FROM  
		DCS_Tank_gauge_trail  
		WHERE (DCS_Tank_gauge_trail.Tank=BoekvoorraadTank.Tank) AND  
		DCS_Tank_gauge_trail.Entry_date >= DATEADD(day,-2,BoekvoorraadTank.Datum) AND  
		DCS_Tank_gauge_trail.Entry_date <= DATEADD(minute,1,BoekvoorraadTank.Datum) ORDER BY DCS_Tank_gauge_trail.Entry_date DESC
		), 
		Dip = (Select top 1 DCS_Tank_gauge_trail.Product_level FROM  
		DCS_Tank_gauge_trail  
		WHERE (DCS_Tank_gauge_trail.Tank=BoekvoorraadTank.Tank) AND  
		DCS_Tank_gauge_trail.Entry_date >= DATEADD(day,-2,BoekvoorraadTank.Datum) AND  
		DCS_Tank_gauge_trail.Entry_date <= DATEADD(minute,1,BoekvoorraadTank.Datum) ORDER BY DCS_Tank_gauge_trail.Entry_date DESC
		), 
		EntisDens = (Select top 1 DCS_Tank_gauge_trail.Product_density FROM  
		DCS_Tank_gauge_trail  
		WHERE (DCS_Tank_gauge_trail.Tank=BoekvoorraadTank.Tank) AND  
		DCS_Tank_gauge_trail.Entry_date >= DATEADD(day,-2,BoekvoorraadTank.Datum) AND  
		DCS_Tank_gauge_trail.Entry_date <= DATEADD(minute,1,BoekvoorraadTank.Datum) ORDER BY DCS_Tank_gauge_trail.Entry_date DESC
		), 
		EntisTemp = (Select top 1 DCS_Tank_gauge_trail.Product_temperature FROM  
		DCS_Tank_gauge_trail  
		WHERE (DCS_Tank_gauge_trail.Tank=BoekvoorraadTank.Tank) AND  
		DCS_Tank_gauge_trail.Entry_date >= DATEADD(day,-2,BoekvoorraadTank.Datum) AND
		DCS_Tank_gauge_trail.Entry_date <= DATEADD(minute,1,BoekvoorraadTank.Datum) ORDER BY DCS_Tank_gauge_trail.Entry_date DESC
		), 
		WaterContentDip = (select Tanks.Water_niveau * 1000 FROM Tanks WHERE Tanks.RecordID = BoekvoorraadTank.Tank)
		where (Entis = 0 OR Entis IS NULL) 
		AND BoekvoorraadTank.Datum = @dtstart
	
		SET @dtstart = DATEADD(day, 1, @dtstart)
	END	

END
