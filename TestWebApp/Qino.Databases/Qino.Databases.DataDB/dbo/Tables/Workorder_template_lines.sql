﻿CREATE TABLE [dbo].[Workorder_template_lines] (
    [RecordID]           INT             IDENTITY (1, 1) NOT NULL,
    [UpdateCounter]      INT             NULL,
    [Workorder_template] INT             NULL,
    [Sequence]           INT             NULL,
    [Name]               VARCHAR (256)   NULL,
    [Days_from_start]    VARCHAR (256)   NULL,
    [Duration_in_days]   VARCHAR (256)   NULL,
    [Question]           VARCHAR (MAX)   NULL,
    [Answer]             INT             NULL,
    [Medewerker]         INT             NULL,
    [Medewerkergroep]    INT             NULL,
    [Bestede_tijd]       NUMERIC (18, 2) NULL,
    [Kosten]             NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_Workorder_template_lines] PRIMARY KEY CLUSTERED ([RecordID] ASC),
    CONSTRAINT [FK_Workorder_template_lines_Medewerker] FOREIGN KEY ([Medewerker]) REFERENCES [dbo].[Medewerker] ([RecordID]) NOT FOR REPLICATION,
    CONSTRAINT [FK_Workorder_template_lines_Medewerker_groep] FOREIGN KEY ([Medewerkergroep]) REFERENCES [dbo].[Medewerker_groep] ([RecordID]) NOT FOR REPLICATION,
    CONSTRAINT [FK_Workorder_template_lines_Workorder_template] FOREIGN KEY ([Workorder_template]) REFERENCES [dbo].[Workorder_template] ([RecordID]) NOT FOR REPLICATION
);


GO
ALTER TABLE [dbo].[Workorder_template_lines] NOCHECK CONSTRAINT [FK_Workorder_template_lines_Medewerker];


GO
ALTER TABLE [dbo].[Workorder_template_lines] NOCHECK CONSTRAINT [FK_Workorder_template_lines_Medewerker_groep];


GO
ALTER TABLE [dbo].[Workorder_template_lines] NOCHECK CONSTRAINT [FK_Workorder_template_lines_Workorder_template];

