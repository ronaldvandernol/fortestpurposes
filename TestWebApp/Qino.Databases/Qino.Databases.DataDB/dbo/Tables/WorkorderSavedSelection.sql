﻿CREATE TABLE [dbo].[WorkorderSavedSelection] (
    [RecordID]      INT           IDENTITY (1, 1) NOT NULL,
    [UpdateCounter] INT           NULL,
    [Name]          VARCHAR (256) NULL,
    [Selection]     VARCHAR (MAX) NULL,
    [LoadAsDefault] BIT           NULL,
    [Date]          DATETIME      CONSTRAINT [DF_WorkorderSavedSelection_Date_] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_WorkorderSavedSelection] PRIMARY KEY CLUSTERED ([RecordID] ASC)
);

