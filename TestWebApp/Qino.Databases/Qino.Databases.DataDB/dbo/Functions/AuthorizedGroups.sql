﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[AuthorizedGroups] 
(	
	-- Add the parameters for the function here
	@groupid int,
	@userid int 
)
RETURNS TABLE 
AS
RETURN 
(
	WITH Authorizations (ParentNodeID, ChildNodeID, Naam, Level)
	AS
	(
	-- Anchor member definition
		SELECT e.ParentMedewerkerGroep, e.RecordID, e.Naam, 0 AS Level
		FROM Medewerker_groep AS e
		WHERE ((ParentMedewerkerGroep IS NULL OR ParentMedewerkerGroep = 0) AND RecordID = @groupid) OR RecordID = @groupid
		UNION ALL
	-- Recursive member definition
		SELECT e.ParentMedewerkerGroep, e.RecordID, e.Naam, Level + 1
		FROM Medewerker_groep AS e
		INNER JOIN Authorizations AS d
			ON e.ParentMedewerkerGroep = d.ChildNodeID
	)

	SELECT ChildNodeID FROM Authorizations
	INNER JOIN Medewerkers_per_groep ON Medewerkers_per_groep.Groep = ChildNodeID
	INNER JOIN Medewerker ON Medewerker.RecordID = Medewerkers_per_groep.Medewerker
	WHERE Medewerker.Gebruiker = @userid
)	

