﻿CREATE FUNCTION AirFactor 
(
	@DensVacL15 NUMERIC(18,4)
)
RETURNS NUMERIC(18,4)
AS
BEGIN
-- =============================================
-- Author:		Geert-Jan Fluitman
-- Create date: 26-07-2016
-- Description:	Return Airfactor
-- =============================================
	DECLARE @ReturnAirFactor NUMERIC(18,4) = 
	  ( SELECT TOP 1
			CASE 
				WHEN Instellingen.UseWeightTable = 1 THEN
					ISNULL(1 - ( SELECT TOP 1 Factor FROM WeightTableToAir WHERE From_density <= @DensVacL15 ORDER BY From_density DESC ),ISNULL(Instellingen.AirFactor,0.0011))
				ELSE ISNULL(Instellingen.AirFactor,0.0011)
			END
		FROM Instellingen 
		WHERE Instellingen.RecordID = 1);

	RETURN @ReturnAirFactor;
END
