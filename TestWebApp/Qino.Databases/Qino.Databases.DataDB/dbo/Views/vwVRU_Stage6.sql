﻿
CREATE VIEW [dbo].[vwVRU_Stage6] AS
(
SELECT 
CAST(row_number() over ( order by vwVRU_Stage5.Client) as INT) RecordID,
CAST(row_number() over ( order by vwVRU_Stage5.Client) as INT) UpdateCounter,
 vwVRU_Stage5.Client AS Client, 
       vwVRU_Stage5.Product AS Product, 
       SUM(((vwVRU_Stage5.RecoverdVolume * VRU_Product.DutyRate) / 100)) as DutyReclaim,
	   vwVRU_Stage5.VRU

FROM vwVRU_Stage5 , VRU_Product
WHERE vwVRU_Stage5.Product = VRU_Product.Product 
--     and vwVRU_Stage5.VRU = [VRU]
GROUP BY vwVRU_Stage5.Client, vwVRU_Stage5.Product, vwVRU_Stage5.VRU
)
